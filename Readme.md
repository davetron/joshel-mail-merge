# Mailmerge from Spreadsheet

## Instructions

1. Upload the spreadsheet you are working on into [Google Drive](http://drive.google.com).
2. Double click the spreadsheet file, and if necessary, click the drop down arrow to import the data into Google Spreadsheets.

![http://puu.sh/j0sx2/96080cf577.png](http://puu.sh/j0sx2/96080cf577.png)
3. Click "Tools>Script Editor"

![http://puu.sh/j0sEm/0245714d6a.png](http://puu.sh/j0sEm/0245714d6a.png)
4. Paste the code from the [this link](https://gitlab.com/garzo1/joshel-mail-merge/raw/master/code.gs) into the text area.
![http://puu.sh/j0sQ5/646e789b32.png](http://puu.sh/j0sQ5/646e789b32.png)
5. Press the "Save" icon to bind this script to the appropriate spreadsheet, and give it a name when prompted.
6. Click the "Play" (triangle) button after checking the file headers (explained below) and modifying the row count (explained below...er)
7. Go back to the Google Drive home and you will see documents have been created, the document names pull from the address field.


# Always check the following!
## Check the file headers
This script assumes the spreadsheets imported will always have the same arrangement of columns to read data from:
* Column K should always contain the street addresses 
* Column F should always contain the last name of your recipient
* Colmnn L should always contain the city
* Column N should always contain the zip code.

Refer to the "2015 06 22 Travis County Probates (TX).xls" you sent me as an example.

## Modify the row count (if needed)
This isn't a fully programmatic solution, so sometimes you might need to edit parts of the code.  I promise this isn't as daunting as it seems.  The 2015 06 22 Travis County Probates (TX).xls file had 16 rows of data that this script parsed and created files.  To ensure the script works correctly, you may need to update row fields depending on the number of rows you have.  You can reaccess the script at any time by opening the spreadsheet and clicking Tools>Script Editor.